/**
 * Created by macbookpro on 18/02/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import DocApp from './DocApp';

// Render the main component into the dom
ReactDOM.render(<DocApp history={browserHistory} />, document.getElementById('root'));
