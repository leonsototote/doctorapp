/**
 * Created by macbookpro on 18/02/17.
 */

import React, { Component } from 'react';
import { Router, Route, IndexRoute } from 'react-router'
import Home from './components/pages/Home';
import Search from './components/pages/Search';
import Stuff from './components/pages/Stuff'
import TemplateDocApp from './components/templates/TemplateDocApp';

class DocApp extends Component {

    // We need to provide a list of routes
    // for our app, and in this case we are
    // doing so from a Root component
    render() {
        return (
            <Router history={this.props.history}>
                <Route path="/" component={TemplateDocApp}>
                    <IndexRoute component={Home}/>
                    <Route path="stuff" component={Stuff} />
                    <Route path="search" component={Search} />
                </Route>
            </Router>
        );
    }
}

export default DocApp;
