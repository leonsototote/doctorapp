/**
 * Created by macbookpro on 18/02/17.
 */

const React = require('react');
import { Link } from 'react-router';
import App from 'grommet/components/App';
import Header from 'grommet/components/Header';
import Menu from 'grommet/components/Menu';
import Anchor from 'grommet/components/Anchor';
import Footer from 'grommet/components/Footer';
import Box from 'grommet/components/Box';
import Label from 'grommet/components/Label';
import FavoriteLogo from 'grommet/components/icons/base/Favorite';

class TemplateDocApp extends React.Component {
    render() {
        return (
        <App centered={false}>

            <Header key="header" size="large"
                    pad={{ horizontal: "medium", between: "small" }}
                    colorIndex="neutral-1"
                    responsive={false} justify="between">
                <Menu inline={false} icon="Apps"
                      dropColorIndex="neutral-1">
                        <Link to="/">Home</Link>
                        <Link to="/stuff">Stuff</Link>
                        <Link to="/search">Search</Link>
                </Menu>
            </Header>

            {this.props.children}

            <Footer float={true} colorIndex="grey-3-a"
                    pad={{vertical: "small", horizontal: "medium", between: "medium"}}
                    wrap={true} direction="row" justify="between" align="center">
                <Box direction="row" align="center" responsive={false}
                     pad={{ between: 'small' }}>
                    <Label size="small" margin="none">Made with</Label>
                    <FavoriteLogo />
                    <Label size="small" margin="none">by </Label>
                    <Anchor>Carlos B)</Anchor>
                </Box>
            </Footer>

        </App>
        );
    }
}

export default TemplateDocApp;
