/**
 * Created by macbookpro on 18/02/17.
 */

const React = require('react');
import Spinning from 'grommet/components/icons/Spinning';
import AnnotatedMeter from 'grommet-addons/components/AnnotatedMeter';

class Home extends React.Component{
    render() {
        return (
            <div>
                <h2>HELLO</h2>
                <p>Cras facilisis urna ornare ex volutpat, et
                    convallis erat elementum. Ut aliquam, ipsum vitae
                    gravida suscipit, metus dui bibendum est, eget rhoncus nibh
                    metus nec massa. Maecenas hendrerit laoreet augue
                    nec molestie. Cum sociis natoque penatibus et magnis
                    dis parturient montes, nascetur ridiculus mus.</p>
                <Spinning />
                <p>Duis a turpis sed lacus dapibus elementum sed eu lectus.</p>
                <AnnotatedMeter type='circle'
                                series={[{"label": "First", "value": 20, "colorIndex": "graph-1"}, {"label": "Second", "value": 50, "colorIndex": "graph-2"}]}
                                legend={true} />
            </div>
        );
    }
}

export default Home;