/**
 * Created by macbookpro on 18/02/17.
 */

const React = require('react');
import EmployeeTable from '../tables/EmployeeTable';

class Stuff extends React.Component{
    render() {
        return (
            <div>
                <h2>STUFF</h2>
                <EmployeeTable employees={EMPLOYEES} />
            </div>
        );
    }
}

var EMPLOYEES = [
    {name: 'Joe Biden', age: 45, years: 5},
    {name: 'President Obama', age: 54, years: 8},
    {name: 'Crystal Mac', age: 34, years: 12},
    {name: 'James Henry', age: 33, years: 2}
];

export default Stuff;