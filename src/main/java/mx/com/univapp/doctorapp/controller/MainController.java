package mx.com.univapp.doctorapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by macbookpro on 02/02/17.
 */

@Controller
public class MainController {

    @RequestMapping("/")
    public String home() {

        return "home";
    }

}
