package mx.com.univapp.doctorapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by macbookpro on 01/02/17.
 */
@SpringBootApplication
public class doctorApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(doctorApplication.class, args);
    }

}
